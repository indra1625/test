import redis
import json


class Redis:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.client = redis.Redis(host=self.host, port=self.port)

    def set_value(self, key, value):
        self.client.set(key, value)

    def get_value(self, key):
        return self.client.get(key)


if __name__ == "__main__":
    config = json.load(open("config.json"))
    connection = Redis(host=config.get("redis").get("host"), port=config.get("redis").get("port"))
    key = str(input("Enter the key \t"))
    value = str(input("Enter the key value\t"))
    connection.set_value(key, value)
    print("key valu here\t", connection.get_value(key))
